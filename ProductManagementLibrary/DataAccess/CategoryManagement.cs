﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ProductManagementLibrary.DataAccess {
    public class CategoryManagement {
        private static CategoryManagement instance = null;
        private static readonly object instanceLock = new object();
        private CategoryManagement() {
        }
        public static CategoryManagement Instance {
            get {
                lock (instanceLock) {
                    if (instance == null) {
                        instance = new CategoryManagement();
                    }
                }
                return instance;
            }
        }

        public IEnumerable<Category> GetCategoryList() {
            List<Category> categories;

            try {
                var myStoreDB = new MyStoreContext();
                categories = myStoreDB.Categories.ToList();
            } catch (Exception ex) {
                throw new Exception(ex.Message);
            }
            return categories;
        }
    }
}
