﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ProductManagementLibrary.DataAccess {
    public class ProductManagement {
        private static ProductManagement instance = null;
        private static readonly object instanceLock = new object();
        private ProductManagement() {
        }
        public static ProductManagement Instance {
            get {
                lock (instanceLock) {
                    if (instance == null) {
                        instance = new ProductManagement();
                    }
                }
                return instance;
            }
        }

        public IEnumerable<Product> GetProductList() {
            List<Product> products;

            try {
                var myStoreDB = new MyStoreContext();
                products = myStoreDB.Products.Include(p => p.Category).ToList();
            } catch (Exception ex) {
                throw new Exception(ex.Message);
            }
            return products;
        }

        public Product GetProductByID(int productID) {
            Product product = null;
            try {
                var myStoreDB = new MyStoreContext();
                product = myStoreDB.Products.SingleOrDefault(product => product.ProductId == productID);
            } catch (Exception ex) {
                throw new Exception(ex.Message);
            }
            return product;
        }

        public void AddNew(Product product) {
            try {
                Product _product = GetProductByID(product.ProductId);
                if (_product == null) {
                    var myStoreDB = new MyStoreContext();
                    myStoreDB.Products.Add(product);
                    myStoreDB.SaveChanges();
                } else {
                    throw new Exception("The product is already exist.");
                }
            } catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public void Update(Product product) {
            try {
                Product p = GetProductByID(product.ProductId);
                if (p != null) {
                    var myStoreDB = new MyStoreContext();
                    myStoreDB.Entry<Product>(product).State = EntityState.Modified;
                    myStoreDB.SaveChanges();
                } else {
                    throw new Exception("The product does not exist.");
                }
            } catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public void Remove(Product product) {
            try {
                Product _product = GetProductByID(product.ProductId);
                if (_product != null) {
                    var myStoreDB = new MyStoreContext();
                    myStoreDB.Products.Remove(product);
                    myStoreDB.SaveChanges();
                } else {
                    throw new Exception("The product does not exist.");
                }
            } catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }
    }
}
