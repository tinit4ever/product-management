﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProductManagementLibrary.DataAccess;

namespace ProductManagementLibrary.Repository {
    public class CategoryRepository: ICategoryRepository {
        public IEnumerable<Category> GetCategories() => CategoryManagement.Instance.GetCategoryList();
    }
}
