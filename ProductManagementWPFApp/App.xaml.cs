﻿using System.Configuration;
using System.Data;
using System.Windows;
using Microsoft.Extensions.DependencyInjection;
using ProductManagementLibrary.Repository;

namespace ProductManagementWPFApp {
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App: Application {
        private ServiceProvider serviceProvider;
        public App() {
            ServiceCollection services = new ServiceCollection();
            ConfigureServices(services);
            serviceProvider = services.BuildServiceProvider();
        }

        private void ConfigureServices(ServiceCollection services) {
            services.AddSingleton(typeof(IProductRepository), typeof(ProductRepository));
            services.AddSingleton(typeof(ICategoryRepository),typeof(CategoryRepository));
            services.AddSingleton<WindowProductManagement>();
        }

        private void OnStartup(object sender, StartupEventArgs a) {
            var windowProductManagement = serviceProvider.GetService<WindowProductManagement>();
            windowProductManagement.Show();
        }
    }

}
