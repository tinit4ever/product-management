﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ProductManagementLibrary.DataAccess;
using ProductManagementLibrary.Repository;

namespace ProductManagementWPFApp {
    /// <summary>
    /// Interaction logic for WindowProductManagement.xaml
    /// </summary>
    public partial class WindowProductManagement: Window {
        IProductRepository productRepository;
        public IEnumerable<Category> categories;
        private bool isCreateMode = false;
        private bool isUpdateMode = false;
        public WindowProductManagement(IProductRepository productRepository, ICategoryRepository categoryRepository) {
            InitializeComponent();
            this.productRepository = productRepository;
            categories = categoryRepository.GetCategories();
            DataContext = this;
            cbCategory.ItemsSource = categories;
            EnableTextBoxes(false);
            txtProductID.IsEnabled = false;
            LoadProductList();
        }
        private void EnableTextBoxes(bool isEnabled) {
            txtProductName.IsEnabled = isEnabled;
            txtUnitsInStock.IsEnabled = isEnabled;
            txtPrice.IsEnabled = isEnabled;
            cbCategory.IsEnabled = isEnabled;
            btnSave.IsEnabled = isEnabled!;
            if (isCreateMode) {
                btnUpdate.IsEnabled = false;
            } else {
                btnUpdate.IsEnabled = true;
            }

            if (isUpdateMode) {
                btnCreate.IsEnabled = false;
            } else {
                btnCreate.IsEnabled = true;
            }
        }

        public void LoadProductList() {
            lvProducts.ItemsSource = productRepository.GetProducts();
        }

        private Product GetProductObject() {
            Product product = null;
            try {
                product = new Product {
                    ProductName = txtProductName.Text,
                    UnitsInStock = short.Parse(txtUnitsInStock.Text),
                    UnitPrice = decimal.Parse(txtPrice.Text),
                };

                Category selectedCategory = (Category)cbCategory.SelectedItem;

                if (selectedCategory != null) {
                    product.CategoryId = selectedCategory.CategoryId;
                }
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, "Get product");
            }
            return product;
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e) {
            if (!isCreateMode) {
                isCreateMode = true;
                EnableTextBoxes(true);
                btnCreate.Content = "Cancel";
            } else {
                isCreateMode = false;
                EnableTextBoxes(false);
                btnCreate.Content = "Create";
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e) {
            if (isCreateMode) {
                try {
                    Product product = GetProductObject();

                    productRepository.InsertProduct(product);
                    LoadProductList();

                    isCreateMode = false;
                    EnableTextBoxes(false);
                    btnCreate.Content = "Create";
                    MessageBox.Show("Created");
                } catch (Exception ex) {
                    MessageBox.Show(ex.Message, "Save Product");
                }
            } else if (isUpdateMode) {
                try {
                    Product product = GetProductObject();
                    product.ProductId = int.Parse(txtProductID.Text);
                    productRepository.UpdateProduct(product);
                    LoadProductList();

                    isUpdateMode = false;
                    EnableTextBoxes(false);
                    btnUpdate.Content = "Update";
                    MessageBox.Show("Updated");
                } catch (Exception ex) {
                    MessageBox.Show(ex.Message, "Save Product");
                }
            }
        }


        private void btnUpdate_Click(object sender, RoutedEventArgs e) {
            if (!isUpdateMode) {
                isUpdateMode = true;
                EnableTextBoxes(true);
                btnUpdate.Content = "Cancel";
            } else {
                isUpdateMode = false;
                EnableTextBoxes(false);
                btnUpdate.Content = "Update";
            }
        }
        private void btnDelete_Click(object sender, RoutedEventArgs e) {
            Product product = GetProductObject() as Product;
            if (string.IsNullOrEmpty(txtProductID.Text)) {
                MessageBox.Show("Please select a product to delete");
            } else {
                product.ProductId = int.Parse(txtProductID.Text);
                productRepository.DeleteProduct(product);
                MessageBox.Show("Deleted");
                LoadProductList();
            }

        }
    }
}
